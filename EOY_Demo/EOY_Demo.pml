<?xml version="1.0" encoding="UTF-8" ?>
<Package name="EOY_Demo" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="choice_sentences" src="behavior_1/Aldebaran/choice_sentences.xml" />
        <File name="swiftswords_ext" src="behavior_1/swiftswords_ext.mp3" />
        <File name="01-05- Happy (From Despicable Me 2)" src="01-05- Happy (From Despicable Me 2).mp3" />
        <File name="Let It Go - Short" src="Let It Go - Short.mp3" />
        <File name="01-06- Wild Blue Yonder (The US Air Force Song)" src="01-06- Wild Blue Yonder (The US Air Force Song).mp3" />
        <File name="OutKast -- Hey Ya lyrics" src="OutKast -- Hey Ya lyrics.mp3" />
        <File name="Taylor Swift - Shake It Off" src="Taylor Swift - Shake It Off.mp3" />
        <File name="The Final Countdown - Europe" src="The Final Countdown - Europe.mp3" />
        <File name="Shortened Eye of the Tiger" src="Shortened Eye of the Tiger.wav" />
        <File name="The Romantics - What I Like About You" src="The Romantics - What I Like About You.wav" />
    </Resources>
    <Topics />
    <IgnoredPaths>
        <Path src="behavior_1/Aldebaran" />
        <Path src="behavior_1" />
        <Path src="01-06- Wild Blue Yonder (The US Air Force Song).mp3" />
        <Path src="manifest.xml" />
        <Path src="behavior_1/swiftswords_ext.mp3" />
        <Path src=".metadata" />
        <Path src="behavior_1/Aldebaran/choice_sentences.xml" />
        <Path src="translations" />
        <Path src="OutKast -- Hey Ya lyrics.mp3" />
        <Path src="The Final Countdown - Europe.mp3" />
        <Path src="Shortened Eye of the Tiger.wav" />
        <Path src="01-05- Happy (From Despicable Me 2).mp3" />
        <Path src="Taylor Swift - Shake It Off.mp3" />
        <Path src="Let It Go - Short.mp3" />
    </IgnoredPaths>
</Package>
