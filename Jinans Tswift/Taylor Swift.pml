<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Taylor Swift" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="ExampleDialog" src="behavior_1/ExampleDialog/ExampleDialog.dlg" />
    </Dialogs>
    <Resources>
        <File name="choice_sentences" src="behavior_1/Aldebaran/choice_sentences.xml" />
        <File name="Love story , Taylor Swift Lyrics on screen" src="Love story , Taylor Swift Lyrics on screen.mp3" />
        <File name="Shake It Off (With Lyrics) - Taylor Swift" src="Shake It Off (With Lyrics) - Taylor Swift.mp3" />
        <File name="Taylor Swift - Look What You Made Me Do (Lyric Video)" src="Taylor Swift - Look What You Made Me Do (Lyric Video).mp3" />
    </Resources>
    <Topics>
        <Topic name="ExampleDialog_enu" src="behavior_1/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
    </Topics>
    <IgnoredPaths />
</Package>
