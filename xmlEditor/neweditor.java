import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.BorderLayout;


public class xmlEditor {

	private JFrame frame;
	//private final Action action = new SwingAction();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					xmlEditor window = new xmlEditor();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public xmlEditor() throws JDOMException, IOException {
		super();
		ArrayList<Group> groups = MakeArray();
		
		initialize(groups);
	}
	
		
	public class Group {
		String name;
		ArrayList<Topic> topics = new ArrayList<Topic>();
	}
	  
	
	public class Topic {
        String name;
        ArrayList<String> questions = new ArrayList<String>();
	}

	public void reorder(JTable table) {
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
        table.setRowSorter(sorter);

        List<RowSorter.SortKey> sortKeys = new ArrayList<>(25);
        //sortKeys.add(new RowSorter.SortKey(4, SortOrder.ASCENDING));
        sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
        sorter.setSortKeys(sortKeys);
				
	}
	
	public ArrayList<Group> MakeArray() throws JDOMException, IOException {
        ArrayList<Group> groups = new ArrayList<Group>();
        {    
                 File inputFile = new File("drksprompts.xml");
                 SAXBuilder saxBuilder = new SAXBuilder();
                 Document document = null;
                 //try {
                 document = saxBuilder.build(inputFile);
                 //} //catch (JDOMException | IOException e) {
                 // TODO Auto-generated catch block
                	 //e.printStackTrace();
                 //}
                 Element classElement = document.getRootElement();
             
                 java.util.List<Element> groupList = classElement.getChildren();
                    
                 for (int temp = 0; temp < groupList.size(); temp++) 
                 {
                    Element group = groupList.get(temp);
                    Group myGroup = new Group();
                    myGroup.name = group.getChild("group_name").getText();


                           {
                                  ArrayList<Topic> topics = new ArrayList<Topic>();
                                  java.util.List<Element> promptList = group.getChildren();
                                        for (int t = 1; t < promptList.size(); t++) {
                                             Element topic = promptList.get(t);
                                             Topic myTopic = new Topic();
                                             myTopic.name = topic.getChild("topic_name").getText();

                                               {
                                                      java.util.List<Element> qList = topic.getChildren();
                                                             for (int tt = 1; tt < (qList.size()); tt++) {
                                                                    Element q = qList.get(tt);
                                                                    myTopic.questions.add(q.getText());

                                                                    }
                                               }
                                          topics.add(myTopic);
                                        
                                        }
                                        myGroup.topics = topics;
                           }
                           
                        groups.add(myGroup);
                 }
        }
        return groups;
        // end change
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(ArrayList<Group> groups){
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel catPanel = new JPanel();
		//JPanel topPanel = new JPanel();
		
		//frame.getContentPane().add(topPanel, BorderLayout.SOUTH);
		
		//topPanel.setVisible(true);
		
		//topPanel.setLayout(null);
		
		String[] columnNames = {"Order", "Prompts"};
		
		DefaultTableModel catTableModel = new DefaultTableModel(columnNames, 0);
		//DefaultTableModel topTableModel = new DefaultTableModel(columnNames, 0);

		/*--------- start of category Panel population ---------*/	
		frame.getContentPane().add(catPanel, BorderLayout.NORTH);
		catPanel.setVisible(true);
		catPanel.setLayout(null);
		
		JTable catTable = new JTable(catTableModel);
		catTable.setVisible(true);
		catTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		catTable.getColumnModel().getColumn(1).setPreferredWidth(300);
		catTable.setBounds(23, 27, 322, 184);
		catPanel.add(catTable);
		
		for (int i = 0; i < groups.size(); i++){
			   String groupsObj = groups.get(i).name;
			   
			   Object[] data = {i+1, groupsObj};
			   
			   catTableModel.addRow(data);
		}
		
		JButton catEdit = new JButton("Edit");
		catEdit.setBounds(23, 222, 89, 23);
		catPanel.add(catEdit);
		
		JButton catOrder = new JButton("Reorder");
		catOrder.setBounds(122, 222, 89, 23);
		catPanel.add(catOrder);

		
		/*--------- end of category Panel population ---------*/		
		
		/*--------- start of topic Panel population ---------*/
		
//		JTable topTable = new JTable(topTableModel);
//		topTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//		topTable.setBounds(23, 27, 322, 184);
//		//topPanel.add(topTable);
//		topPanel.add(topTable);
//				
//		JButton topEdit = new JButton("Edit");
//		topEdit.setBounds(23, 222, 89, 23);
//		topPanel.add(topEdit);
//				
//		JButton topOrder = new JButton("Reorder");
//		topOrder.setBounds(122, 222, 89, 23);
//		topPanel.add(topOrder);
//		topOrder.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseClicked(MouseEvent arg0) {
//				reorder(topTable);
//			        topTableModel.fireTableDataChanged();
//			}
//		});		
		/*--------- end of topic Panel population ---------*/
		
		
		//JScrollPane topicScroll = new JScrollPane(topTable);
							
	}
		
}
