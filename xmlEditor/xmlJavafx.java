package application;
	
import java.io.File;
import java.io.IOException;

import java.util.ArrayList;

//from ww w . jav  a 2s. c om
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

public class xmlEditor extends Application {
public static void main(String[] args) {
  launch(args);
}

public class ArrayGroup {
	String name;
	ArrayList<Topic> topics = new ArrayList<Topic>();
}
  

public class Topic {
    String name;
    ArrayList<String> questions = new ArrayList<String>();
}

public ArrayList<ArrayGroup> MakeArray() throws JDOMException, IOException {
    ArrayList<ArrayGroup> groups = new ArrayList<ArrayGroup>();
    {    
             File inputFile = new File("drksprompts.xml");
             SAXBuilder saxBuilder = new SAXBuilder();
             Document document = null;
             //try {
             document = saxBuilder.build(inputFile);
             //} //catch (JDOMException | IOException e) {
             // TODO Auto-generated catch block
            	 //e.printStackTrace();
             //}
             Element classElement = document.getRootElement();
         
             java.util.List<Element> groupList = classElement.getChildren();
                
             for (int temp = 0; temp < groupList.size(); temp++) 
             {
                Element group = groupList.get(temp);
                ArrayGroup myGroup = new ArrayGroup();
                myGroup.name = group.getChild("group_name").getText();


                       {
                              ArrayList<Topic> topics = new ArrayList<Topic>();
                              java.util.List<Element> promptList = group.getChildren();
                                    for (int t = 1; t < promptList.size(); t++) {
                                         Element topic = promptList.get(t);
                                         Topic myTopic = new Topic();
                                         myTopic.name = topic.getChild("topic_name").getText();

                                           {
                                                  java.util.List<Element> qList = topic.getChildren();
                                                         for (int tt = 1; tt < (qList.size()); tt++) {
                                                                Element q = qList.get(tt);
                                                                myTopic.questions.add(q.getText());

                                                                }
                                           }
                                      topics.add(myTopic);
                                    
                                    }
                                    myGroup.topics = topics;
                       }
                       
                    groups.add(myGroup);
             }
    }
    return groups;
    // end change
}
private TreeItem<File> createNode(final File f) {
  return new TreeItem<File>(f) {
    private boolean isLeaf;
    private boolean isFirstTimeChildren = true;
    private boolean isFirstTimeLeaf = true;

    @Override
    public ObservableList<TreeItem<File>> getChildren() {
      if (isFirstTimeChildren) {
        isFirstTimeChildren = false;
        super.getChildren().setAll(buildChildren(this));
      }
      return super.getChildren();
    }

    @Override
    public boolean isLeaf() {
      if (isFirstTimeLeaf) {
        isFirstTimeLeaf = false;
        File f = (File) getValue();
        isLeaf = f.isFile();
      }
      return isLeaf;
    }

    private ObservableList<TreeItem<File>> buildChildren(
        TreeItem<File> TreeItem) {
      File f = TreeItem.getValue();
      if (f == null) {
        return FXCollections.emptyObservableList();
      }
      if (f.isFile()) {
        return FXCollections.emptyObservableList();
      }
      File[] files = f.listFiles();
      if (files != null) {
        ObservableList<TreeItem<File>> children = FXCollections
            .observableArrayList();
        for (File childFile : files) {
          children.add(createNode(childFile));
        }
        return children;
      }
      return FXCollections.emptyObservableList();
    }
  };
}

@Override
public void start(Stage stage) {
  Scene scene = new Scene(new Group(), 300, 300);
  VBox vbox = new VBox();
  
  try {
  
  File xmlFile = new File("drksprompts.xml");
  DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
  DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
  org.w3c.dom.Document doc = dBuilder.parse(xmlFile);
  doc.getDocumentElement().normalize();

  TreeItem<File> root = createNode(new File("drksprompts.xml"));
  TreeView treeView = new TreeView<File>(root);

  vbox.getChildren().add(treeView);
  ((Group) scene.getRoot()).getChildren().add(vbox);

  stage.setScene(scene);
  stage.show();
  } catch (Exception e) {
	  e.printStackTrace();
  }
  }
}
